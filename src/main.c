#include "test.h"
#include <stdio.h>

int main(int argc, char const* argv[]) {
    (void)argc; (void)argv;
    fprintf(stdout, "Passed %d from 5 tests", test_malloc() + free_one_from_two_test() + free_two_blocks_from_many_test() + extended_old_test() + not_extend_old_test());
    return 0;
}
