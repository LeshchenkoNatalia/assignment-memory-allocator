#ifndef TEST_H
#define TEST_H

int test_malloc();
int free_one_from_two_test();
int free_two_blocks_from_many_test();
int extended_old_test();
int not_extend_old_test();

#endif // TEST_H
