#include "mem.h"
#include "util.h"
#include "test.h"
#include "mem_internals.h"

#include <stdio.h>

#define SIZE 1000

static inline void clear(void* heap, size_t size) {
    munmap(heap, size_from_capacity((block_capacity) { size }).bytes);
}

static struct block_header* get_block(void* heap, size_t kol) {
    struct block_header* head = heap;
    size_t k = 1;
    while (k < kol) {
        head = head->next;
        k++;
    }
    return head;
}


int test_malloc() {
    fprintf(stdout, "Test 1\n");
    struct block_header* heap = (struct block_header*)heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void* data = _malloc(SIZE);
    debug_heap(stdout, heap);
    if (get_block(heap, 1)->is_free != false || get_block(heap, 1)->capacity.bytes != SIZE || !data) {
        fprintf(stdout, "Test 1 failed\n");
        clear(heap, REGION_MIN_SIZE);
        return 0;
    }
    _free(data);
    clear(heap, REGION_MIN_SIZE);
    fprintf(stdout, "Test 1 passed\n");
    return 1;
}

int free_one_from_two_test() {
    fprintf(stdout, "Test 2\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void* first = _malloc(SIZE);
    void* second = _malloc(SIZE);
    debug_heap(stdout, heap);
    _free(first);
    if (get_block(heap, 1)->is_free == false) {
        fprintf(stdout, "Test 2 failed\n");
        clear(heap, REGION_MIN_SIZE);
        return 0;
    }
    debug_heap(stdout, heap);
    _free(second);
    clear(heap, REGION_MIN_SIZE);
    fprintf(stdout, "Test 2 passed\n");
    return 1;
}

int free_two_blocks_from_many_test() {
    fprintf(stdout, "Test 3\n");
    struct block_header* heap = (struct block_header*)heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void* first = _malloc(SIZE);
    void* second = _malloc(SIZE);
    void* third = _malloc(SIZE);
    debug_heap(stdout, heap);
    _free(first);
    _free(second);
    if (get_block(heap, 1)->is_free == false || get_block(heap, 2)->is_free != false) {
        fprintf(stdout, "Test 3 failed\n");
        clear(heap, REGION_MIN_SIZE);
        return 0;
    }
    debug_heap(stdout, heap);
    _free(third);
    clear(heap, REGION_MIN_SIZE);
    fprintf(stdout, "Test 3 passed\n");
    return 1;
}

int extended_old_test() {
    fprintf(stdout, "Test 4\n");
    struct block_header* heap = (struct block_header*)heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void* data = _malloc(REGION_MIN_SIZE + 1);
    if (get_block(heap, 1)->capacity.bytes != REGION_MIN_SIZE + 1) {
        fprintf(stdout, "Test 4 failed\n");
        _free(data);
        clear(heap, REGION_MIN_SIZE + 1);
        return 0;
    }
    debug_heap(stdout, heap);
    _free(data);
    clear(heap, REGION_MIN_SIZE + 1);
    fprintf(stdout, "Test 4 passed\n");
    return 1;
}

int not_extend_old_test() {
    fprintf(stdout, "Test 5\n");
    struct block_header* heap = (struct block_header*)heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void* first = _malloc(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    struct block_header* block = first - offsetof(struct block_header, contents);
    void* addr = block->contents + block->capacity.bytes;
    void* tmp = mmap(addr, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    void* second = _malloc(REGION_MIN_SIZE);
    if (get_block(heap, 2)->capacity.bytes != REGION_MIN_SIZE || get_block(heap, 2)->is_free != false || heap == tmp) {
        fprintf(stdout, "Test 5 failed\n");
        _free(first);
        _free(second);
        clear(heap, REGION_MIN_SIZE);
        clear(tmp, REGION_MIN_SIZE);
        return 0;
    }
    debug_heap(stdout, heap);
    _free(first);
    _free(second);
    clear(heap, REGION_MIN_SIZE);
    clear(tmp, REGION_MIN_SIZE);
    fprintf(stdout, "Test 5 passed\n");
    return 1;
}
